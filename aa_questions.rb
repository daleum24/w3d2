require 'singleton'
require 'sqlite3'

class QuestionsDatabase < SQLite3::Database
  include Singleton

  def initialize
    super("questions.db")

    self.results_as_hash = true
    self.type_translation = true
  end
end


class Question
  attr_accessor :id, :title, :body, :author_id

  def initialize(params = {})
    @id, @title, @body, @author_id = params["id"]  , params["title"],
                                     params["body"], params["author_id"]
  end

  def find_by_id
    results = QuestionsDatabase.instance.execute(<<-SQL, self.id)
      SELECT
        *
      FROM
        questions
      WHERE
        questions.id = ?
    SQL

    results.map {|result| Question.new(result)}
  end
end

class User
  attr_accessor :id, :fname, :lname

  def initialize(params = {})
    @id, @fname, @lname = params["id"], params["fname"], params["lname"]
  end

  def find_by_id
    results = QuestionsDatabase.instance.execute(<<-SQL, self.id)
      SELECT
        *
      FROM
        users
      WHERE
        users.id = ?
    SQL

    results.map {|result| User.new(result)}
  end
end

class QuestionFollower
  attr_accessor :id, :user_id, :question_id

  def initialize(params = {})
    @id, @user_id, @question_id = params["id"], params["user_id"],
                                  params["question_id"]
  end

  def find_by_id
    results = QuestionsDatabase.instance.execute(<<-SQL, self.id)
      SELECT
        *
      FROM
        question_followers
      WHERE
        question_followers.id = ?
    SQL

    results.map {|result| QuestionFollower.new(result)}
  end
end

class QuestionLike
  attr_accessor :id, :user_id, :question_id

  def initialize(params = {})
    @id, @user_id, @question_id = params["id"], params["user_id"],
                                  params["question_id"]
  end

  def find_by_id
    results = QuestionsDatabase.instance.execute(<<-SQL, self.id)
      SELECT
        *
      FROM
        question_likes
      WHERE
        question_likes.id = ?
    SQL

    results.map {|result| QuestionLike.new(result)}
  end
end

class Reply
  attr_accessor :id, :subject_question_id, :parent_reply, :author_id, :body

  def initialize(params = {})
    @id, @subject_question_id, @parent_reply, @author_id, @body =
          params["id"], params["subject_question_id"], params["parent_reply"],
          params["author_id"], params["body"]
  end

  def find_by_id
    results = QuestionsDatabase.instance.execute(<<-SQL, self.id)
      SELECT
        *
      FROM
        replies
      WHERE
        replies.id = ?
    SQL

    results.map {|result| Reply.new(result)}
  end
end