CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  fname VARCHAR(255) NOT NULL,
  lname VARCHAR(255) NOT NULL
);

CREATE TABLE questions (
  id INTEGER PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  body VARCHAR(255) NOT NULL,
  author_id INTEGER NOT NULL,
  FOREIGN KEY(author_id) REFERENCES users(id)
);

CREATE TABLE question_followers (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  FOREIGN KEY(user_id) REFERENCES users(id),
  FOREIGN KEY(question_id) REFERENCES questions(id)
);

CREATE TABLE replies (
  id INTEGER PRIMARY KEY,
  subject_question_id INTEGER NOT NULL,
  parent_reply INTEGER,
  author_id INTEGER NOT NULL,
  body VARCHAR(255) NOT NULL,
  FOREIGN KEY(subject_question_id) REFERENCES questions(id),
  FOREIGN KEY(author_id) REFERENCES user(id)
);

CREATE TABLE question_likes (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  FOREIGN KEY(user_id) REFERENCES users(id),
  FOREIGN KEY(question_id) REFERENCES questions(id)
);


INSERT INTO
  users(fname, lname)
VALUES
  ("Dale", "Um"), ("Brian", "Deane"), ("Sid", "Raval"), ("Tommy", "Duek");


INSERT INTO
  questions(title, body, author_id)
VALUES
  ("SO LOST", "WHERE AM I????", (SELECT id FROM users WHERE fname = 'Brian')),
  ("SO FUCKING LOST", "WHERE AM I????", (SELECT id FROM users WHERE fname = 'Brian')),
  ("SO HUNGRY", "WHEN'S LUNCH???", (SELECT id FROM users WHERE fname = 'Dale')),
  ("SO FUCKING HUNGRY", "WHEN'S LUNCH???", (SELECT id FROM users WHERE fname = 'Dale'));


INSERT INTO
  question_followers(user_id, question_id)
VALUES
  ((SELECT id FROM users WHERE fname = "Sid"),
   (SELECT id FROM questions WHERE title = "SO HUNGRY")),
  ((SELECT id FROM users WHERE fname = 'Brian'),
   (SELECT id FROM questions WHERE title = "SO HUNGRY")),
  ((SELECT id FROM users WHERE fname = 'Dale'),
   (SELECT id FROM questions WHERE title = "SO LOST"));

INSERT INTO
  replies(subject_question_id, parent_reply, author_id, body)
VALUES
  ((SELECT id FROM questions WHERE title LIKE "SO HUNGRY"),
    NULL,
   (SELECT id FROM users WHERE fname = "Tommy"),
    "YOLO");

INSERT INTO
  replies(subject_question_id, parent_reply, author_id, body)
VALUES
  ((SELECT id FROM questions WHERE title LIKE "SO HUNGRY"),
   (SELECT id FROM replies WHERE body = "YOLO"),
   (SELECT id FROM users WHERE fname = "Sid"),
    "SQL ZOOOOOO");

INSERT INTO
  question_likes(user_id, question_id)
VALUES
  ((SELECT id FROM users WHERE fname = "Tommy"),
  (SELECT id FROM questions WHERE title LIKE "SO HUNGRY"));

INSERT INTO
  question_likes(user_id, question_id)
VALUES
  ((SELECT id FROM users WHERE fname = "Brian"),
  (SELECT id FROM questions WHERE title LIKE "SO HUNGRY"));

INSERT INTO
  question_likes(user_id, question_id)
VALUES
  ((SELECT id FROM users WHERE fname = "Sid"),
  (SELECT id FROM questions WHERE title LIKE "SO HUNGRY"));




