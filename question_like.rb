require_relative 'requires'

class QuestionLike

  def self.find_by_id(id)
    results = QuestionsDatabase.instance.execute(<<-SQL, id)
      SELECT
        *
      FROM
        question_likes
      WHERE
        question_likes.id = ?
    SQL

    results.map {|result| QuestionLike.new(result)}[0]
  end

  def self.likers_for_question_id(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, question_id)
      SELECT
        users.*
      FROM
        question_likes
          JOIN
            users ON question_likes.user_id = users.id
      WHERE
        question_likes.question_id = ?
    SQL

    results.map {|result| User.new(result)}
  end

  def self.num_likes_for_question_id(question_id)
    count = QuestionsDatabase.instance.execute(<<-SQL, question_id)
      SELECT
        COUNT(*)
      FROM
        question_likes
      WHERE
        question_likes.question_id = ?
      GROUP BY question_id
    SQL

    count[0].values.last
  end

  def self.liked_questions_for_user_id(user_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, user_id)
      SELECT
        questions.*
      FROM
        question_likes
          JOIN
            questions ON question_likes.question_id = questions.id
      WHERE
        question_likes.user_id = ?
    SQL

    results.map {|result| Question.new(result)}
  end

  def self.most_liked_questions(n)
    results = QuestionsDatabase.instance.execute(<<-SQL)
      SELECT
        questions.*
      FROM
        question_likes
        JOIN
          questions ON question_likes.question_id = questions.id
      GROUP BY
        question_id
    SQL

    questions = results.map { |result| Question.new(result) }
    n = questions.size if n > questions.size
    questions[-n..-1].reverse
  end

  attr_accessor :id, :user_id, :question_id

  def initialize(params = {})
    @id, @user_id, @question_id = params["id"], params["user_id"],
                                  params["question_id"]
  end

end
