require_relative 'requires'

class QuestionFollower

  def self.find_by_id(id)
    results = QuestionsDatabase.instance.execute(<<-SQL, id)
      SELECT
        *
      FROM
        question_followers
      WHERE
        question_followers.id = ?
    SQL

    results.map {|result| QuestionFollower.new(result)}[0]
  end

  def self.followers_for_question_id(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, question_id)
      SELECT
        users.*
      FROM
        question_followers
          JOIN
            users ON question_followers.user_id = users.id
      WHERE
        question_followers.question_id = ?
    SQL

    results.map { |result| User.new(result) }
  end

  def self.most_followed_questions(n)
    results = QuestionsDatabase.instance.execute(<<-SQL)
      SELECT
        questions.*
      FROM
        question_followers
        JOIN
          questions ON question_followers.question_id = questions.id
      GROUP BY
        question_id
    SQL

    questions = results.map { |result| Question.new(result) }
    n = questions.size if n > questions.size
    questions[-n..-1].reverse
  end

  def self.followed_for_user_id(user_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, user_id)
      SELECT
        questions.*
      FROM
        question_followers
          JOIN
            questions ON question_followers.question_id = questions.id
      WHERE
        question_followers.user_id = ?
    SQL

    results.map { |result| Question.new(result) }
  end

  attr_accessor :id, :user_id, :question_id

  def initialize(params = {})
    @id, @user_id, @question_id = params["id"], params["user_id"],
                                  params["question_id"]
  end
end