require_relative 'requires'

class Reply

  def self.find_by_id(id)
    results = QuestionsDatabase.instance.execute(<<-SQL, id)
      SELECT
        *
      FROM
        replies
      WHERE
        replies.id = ?
    SQL

    results.map {|result| Reply.new(result)}[0]
  end

  def self.find_by_author_id(author_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, author_id)
      SELECT
        *
      FROM
        replies
      WHERE
        replies.author_id = ?
    SQL

    results.map {|result| Reply.new(result)}
  end

  def self.find_by_question_id(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, question_id)
      SELECT
        *
      FROM
        replies
      WHERE
        replies.subject_question_id = ?
    SQL

    results.map {|result| Reply.new(result)}
  end

  attr_accessor :id, :subject_question_id, :parent_reply, :author_id, :body

  def initialize(params = {})
    @id, @subject_question_id, @parent_reply, @author_id, @body =
          params["id"], params["subject_question_id"], params["parent_reply"],
          params["author_id"], params["body"]
  end

  #Sqlite3: problem when trying to interpolate nil into NULL (parent_reply column)
  def save
    if self.id.nil?
      QuestionsDatabase.instance.execute(<<-SQL, self.subject_question_id, self.parent_reply, self.author_id, self.body)
        INSERT INTO
          replies (subject_question_id, parent_reply, author_id, body)
        VALUES
          (?, ?, ?, ?)
        SQL
      @id = QuestionsDatabase.instance.last_insert_row_id

    else
      fake_parent = parent_reply || 'NULL'
      QuestionsDatabase.instance.execute(<<-SQL, self.subject_question_id,  self.author_id, self.body, self.id)
        UPDATE
          replies
        SET
           subject_question_id = ?,
           author_id           = ?,
           body                = ?
        WHERE
          id = ?
        SQL
    end
  end

  def author
    User.find_by_id(self.author_id)
  end

  def question
    Question.find_by_id(self.subject_question_id)
  end

  def parent_reply
    # replies can be null - we preempt that
    return [] if @parent_reply.nil?
    Reply.find_by_id(@parent_reply)
  end

  def child_replies
    results = QuestionsDatabase.instance.execute(<<-SQL, self.id)
      SELECT
        *
      FROM
        replies
      WHERE
        replies.parent_reply = ?
    SQL

    results.map { |result| Reply.new(result) }
  end
end