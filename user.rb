require_relative 'requires'

class User < Table

  def self.find_by_name(fname, lname)
    results = QuestionsDatabase.instance.execute(<<-SQL, fname, lname)
      SELECT
        *
      FROM
        users
      WHERE
        users.fname = ? AND users.lname = ?
    SQL

    results.map { |result| User.new(result) }[0]
  end

  def self.find_by_id(id)
    results = QuestionsDatabase.instance.execute(<<-SQL, id)
      SELECT
        *
      FROM
        users
      WHERE
        users.id = ?
    SQL

    results.map {|result| User.new(result)}[0]
  end

  attr_accessor :id, :fname, :lname

  def initialize(params = {})
    @id, @fname, @lname = params["id"], params["fname"], params["lname"]
  end

  def save
    if self.id.nil?
      QuestionsDatabase.instance.execute(<<-SQL, self.fname, self.lname)
            INSERT INTO
              users (fname, lname)
            VALUES
              (?, ?)
          SQL

      @id = QuestionsDatabase.instance.last_insert_row_id

    else
      QuestionsDatabase.instance.execute(<<-SQL, self.fname, self.lname, self.id)
          UPDATE
            users
          SET
            fname = ?,
            lname = ?
          WHERE
            id = ?
        SQL
    end

  end

  def authored_questions
    Question.find_by_author_id(self.id)
  end

  def authored_replies
    Reply.find_by_author_id(self.id)
  end

  def followed_questions
    QuestionFollower.followed_for_user_id(self.id)
  end

  def liked_questions
    QuestionLike.liked_questions_for_user_id(self.id)
  end

  def average_karma
    count = QuestionsDatabase.instance.execute(<<-SQL, self.id)
    SELECT
      COUNT(user_id), COUNT(DISTINCT id)
    FROM
      (SELECT
        q.id, ql.user_id
       FROM
         users u
          JOIN
            questions q ON u.id = q.author_id
            LEFT OUTER JOIN
              question_likes ql ON q.id = ql.question_id
        WHERE
          u.id = ?)
      SQL

    count[0].values[0].to_f / count[0].values[1]
  end
end